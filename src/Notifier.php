<?php

declare(strict_types=1);

namespace Drupal\notifier;

use Drupal\notifier\ChannelRouting\Channels\Channels;
use Symfony\Component\DependencyInjection\Attribute\AutowireIterator;
use Symfony\Component\Notifier\Channel\ChannelInterface;
use Symfony\Component\Notifier\Notification\Notification;
use Symfony\Component\Notifier\NotifierInterface;
use Symfony\Component\Notifier\Recipient\NoRecipient;
use Symfony\Component\Notifier\Recipient\RecipientInterface;
use Symfony\Contracts\Service\ServiceCollectionInterface;

/**
 * Reworks Notifier service to take on Drupal concepts and expectations.
 *
 * Default notifier implementation is final and even if it wasn't, does not
 * provide suitable extension points.
 *
 * @see \Symfony\Component\Notifier\Notifier
 */
final class Notifier implements NotifierInterface {

  /**
   * @phpstan-param \Traversable<\Drupal\notifier\ChannelRouting\ChannelRoutingInterface> $channelRouting
   * @phpstan-param \Symfony\Contracts\Service\ServiceCollectionInterface<\Symfony\Component\Notifier\Channel\ChannelInterface> $channels
   */
  public function __construct(
    #[AutowireIterator(tag: 'notifier.channel_routing')]
    protected \Traversable $channelRouting,
    private ServiceCollectionInterface $channels,
  ) {
  }

  public function send(Notification $notification, RecipientInterface ...$recipients): void {
    if (\count($recipients) === 0) {
      $recipients = [new NoRecipient()];
    }

    foreach ($recipients as $recipient) {
      foreach ($this->getChannels($notification, $recipient) as $channel => $transportName) {
        $channel->notify($notification, $recipient, $transportName);
      }
    }
  }

  /**
   * @phpstan-return iterable<\Symfony\Component\Notifier\Channel\ChannelInterface, string|null>
   */
  private function getChannels(Notification $notification, RecipientInterface $recipient): iterable {
    $channels = Channels::create(
      data: \array_keys(\iterator_to_array($this->channels)),
      channelGetter: fn (string $channelName): ChannelInterface => $this->channels->has($channelName) ? $this->channels->get($channelName) : throw new \LogicException(\sprintf('Channel %s does not exist', $channelName)),
    );

    foreach ($this->channelRouting as $channelRouter) {
      $channelRouter->channels($channels, $notification, $recipient);
    }

    foreach ($channels as $channel) {
      yield $channels->getChannel($channel) => $channel->transport;
    }
  }

}
