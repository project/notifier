<?php

declare(strict_types=1);

namespace Drupal\notifier\Recipients\Recipient;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\EmailItem;
use Drupal\user\UserInterface;
use Symfony\Component\Notifier\Recipient\EmailRecipientInterface;

/**
 * An implementation of Email Recipient for the exclusive use by Notifier.
 *
 * You should implement your own class implementing EmailRecipientInterface if
 * you need an object like this.
 */
final class EmailRecipient implements EmailRecipientInterface {

  /**
   * @internal
   * @phpstan-param non-empty-string $email
   */
  private function __construct(
    private readonly string $email,
    private readonly EntityInterface $entity,
  ) {
  }

  /**
   * @internal
   * @throws \InvalidArgumentException
   */
  public static function fromUser(
    UserInterface $user,
  ): static {
    $email = $user->getEmail();
    return (\is_string($email) && $email !== '')
      ? new static($email, $user)
      : throw new \InvalidArgumentException('Email missing');
  }

  /**
   * @throws \InvalidArgumentException
   * @internal
   */
  public static function fromEmailItem(
    EmailItem $fieldItem,
  ): static {
    $email = $fieldItem->getString();
    return $email !== '' ? new static($email, $fieldItem->getEntity()) : throw new \InvalidArgumentException('Email missing');
  }

  public function getEmail(): string {
    return $this->email;
  }

  public function getEntity(): EntityInterface {
    return $this->entity;
  }

}
