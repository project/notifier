<?php

declare(strict_types=1);

namespace Drupal\notifier\Recipients\Recipient;

use Drupal\Core\Entity\EntityInterface;
use Drupal\telephone\Plugin\Field\FieldType\TelephoneItem;
use Symfony\Component\Notifier\Recipient\SmsRecipientInterface;

/**
 * An implementation of SMS Recipient for the exclusive use by Notifier.
 *
 * You should implement your own class implementing SmsRecipientInterface if
 * you need an object like this.
 */
final class SmsRecipient implements SmsRecipientInterface {

  /**
   * @internal
   * @phpstan-param non-empty-string $phoneNumber
   */
  private function __construct(
    private readonly string $phoneNumber,
    private readonly EntityInterface $entity,
  ) {
  }

  /**
   * @internal
   * @throws \InvalidArgumentException
   */
  public static function fromTelephoneItem(
    TelephoneItem $fieldItem,
  ): static {
    $phoneNumber = $fieldItem->getString();
    return $phoneNumber !== '' ? new static($phoneNumber, $fieldItem->getEntity()) : throw new \InvalidArgumentException('Phone number missing');
  }

  public function getPhone(): string {
    return $this->phoneNumber;
  }

  public function getEntity(): EntityInterface {
    return $this->entity;
  }

}
