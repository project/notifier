<?php

declare(strict_types=1);

namespace Drupal\notifier\Recipients;

/**
 * Interface for the service to get recipients for a given object.
 */
interface NotifierRecipientsInterface {

  /**
   * @phpstan-return iterable<\Symfony\Component\Notifier\Recipient\RecipientInterface>
   */
  public function getRecipients(object $for, ?RecipientQuery $query = NULL): iterable;

}
