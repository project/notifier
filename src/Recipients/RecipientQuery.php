<?php

declare(strict_types=1);

namespace Drupal\notifier\Recipients;

final class RecipientQuery {

  private const USE_NAMED_PARAMETERS = 'Constructing this object without named parameters is not supported.';

  /**
   * Creates a new recipient query.
   *
   * @param ?string $useNamedParameters
   *   Always use named parameters!
   * @param class-string<\Symfony\Component\Notifier\Recipient\RecipientInterface> $implementing
   *   A class string extending RecipientInterface specifying which interface
   *   recipients added to Recipients::addRecipient must implement.
   */
  public function __construct(
    ?string $useNamedParameters = self::USE_NAMED_PARAMETERS,
    public readonly ?string $implementing = NULL,
  ) {
    if (self::USE_NAMED_PARAMETERS !== $useNamedParameters) {
      throw new \LogicException(self::USE_NAMED_PARAMETERS);
    }
  }

  public function doesObjectImplementRequiredInterface(object $for): bool {
    if ($this->implementing === NULL) {
      return TRUE;
    }

    return (new \ReflectionClass($for))->implementsInterface($this->implementing);
  }

  /**
   * @internal
   */
  public static function defaults(): static {
    return new static();
  }

}
