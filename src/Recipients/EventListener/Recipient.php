<?php

declare(strict_types=1);

namespace Drupal\notifier\Recipients\EventListener;

use Drupal\notifier\Recipients\Event\Recipients;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Notifier\Recipient\RecipientInterface;

/**
 * Gets recipients from objects implementing RecipientInterface.
 *
 * For example, bundle-class entities.
 *
 * @see \Symfony\Component\Notifier\Recipient\RecipientInterface
 */
final readonly class Recipient implements EventSubscriberInterface {

  public function onRecipients(Recipients $event): void {
    if ($event->for instanceof RecipientInterface) {
      $event->addRecipient($event->for);
    }
  }

  public static function getSubscribedEvents(): array {
    return [Recipients::class => 'onRecipients'];
  }

}
