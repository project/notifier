<?php

declare(strict_types=1);

namespace Drupal\notifier\Recipients\EventListener;

use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\notifier\Recipients\Event\Recipients;
use Drupal\notifier\Recipients\Recipient\SmsRecipient;
use Drupal\telephone\Plugin\Field\FieldType\TelephoneItem;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Gets phone numbers from configured entity fields.
 *
 * See README for configuration details.
 */
final readonly class EntityPhoneNumbers implements EventSubscriberInterface {

  /**
   * @var iterable<\Drupal\notifier\Recipients\BundleFieldConfiguration>
   */
  private iterable $configurations;

  /**
   * @internal
   */
  public function __construct(
    string $configurations,
  ) {
    // @phpstan-ignore-next-line
    $this->configurations = \unserialize($configurations);
  }

  public function onPhoneNumbers(Recipients $event): void {
    $entity = $event->for;
    if (!$entity instanceof FieldableEntityInterface) {
      return;
    }

    foreach ($this->configurations as $configuration) {
      foreach ($entity->get($configuration->fieldName) as $item) {
        \assert($item instanceof TelephoneItem);
        if ($item->isEmpty() === FALSE) {
          try {
            $event->addRecipient(SmsRecipient::fromTelephoneItem($item));
          }
          catch (\InvalidArgumentException) {
          }
        }
      }
    }
  }

  public static function getSubscribedEvents(): array {
    return [Recipients::class => 'onPhoneNumbers'];
  }

}
