<?php

declare(strict_types=1);

namespace Drupal\notifier\Recipients\EventListener;

use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\EmailItem;
use Drupal\notifier\Recipients\Event\Recipients;
use Drupal\notifier\Recipients\Recipient\EmailRecipient;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Gets emails from configured entity fields.
 *
 * See README for configuration details.
 */
final readonly class EntityEmails implements EventSubscriberInterface {

  /**
   * @var iterable<\Drupal\notifier\Recipients\BundleFieldConfiguration>
   */
  private iterable $configurations;

  /**
   * @internal
   */
  public function __construct(
    string $configurations,
  ) {
    // @phpstan-ignore-next-line
    $this->configurations = \unserialize($configurations);
  }

  public function onEmails(Recipients $event): void {
    $entity = $event->for;

    if (!$entity instanceof FieldableEntityInterface) {
      return;
    }

    foreach ($this->configurations as $configuration) {
      foreach ($entity->get($configuration->fieldName) as $item) {
        \assert($item instanceof EmailItem);
        if ($item->isEmpty() === FALSE) {
          try {
            $event->addRecipient(EmailRecipient::fromEmailItem($item));
          }
          catch (\InvalidArgumentException) {
          }
        }
      }
    }
  }

  public static function getSubscribedEvents(): array {
    return [Recipients::class => 'onEmails'];
  }

}
