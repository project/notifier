<?php

declare(strict_types=1);

namespace Drupal\notifier\Recipients\EventListener;

use Drupal\notifier\Recipients\Event\Recipients;
use Drupal\notifier\Recipients\Recipient\EmailRecipient;
use Drupal\user\UserInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Gets emails from user email method/field.
 */
final class UserEmail implements EventSubscriberInterface {

  public function onUserEmail(Recipients $event): void {
    if (!$event->for instanceof UserInterface) {
      return;
    }

    try {
      $event->addRecipient(EmailRecipient::fromUser($event->for));
    }
    catch (\InvalidArgumentException) {
    }
  }

  public static function getSubscribedEvents(): array {
    return [Recipients::class => 'onUserEmail'];
  }

}
