<?php

declare(strict_types=1);

namespace Drupal\notifier\Recipients;

use Ramsey\Collection\AbstractCollection;

/**
 * @extends \Ramsey\Collection\AbstractCollection<\Symfony\Component\Notifier\Recipient\RecipientInterface>
 */
final class Recipients extends AbstractCollection {

  public function getType(): string {
    return '\\Symfony\\Component\\Notifier\\Recipient\\RecipientInterface';
  }

}
