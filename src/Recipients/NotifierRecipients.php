<?php

declare(strict_types=1);

namespace Drupal\notifier\Recipients;

use Drupal\notifier\Recipients\Event\Recipients as RecipientsEvent;
use Psr\EventDispatcher\EventDispatcherInterface;

final class NotifierRecipients implements NotifierRecipientsInterface {

  public function __construct(
    private EventDispatcherInterface $eventDispatcher,
  ) {
  }

  public function getRecipients(object $for, ?RecipientQuery $query = NULL): iterable {
    $this->eventDispatcher->dispatch($event = new RecipientsEvent($for, $query ?? RecipientQuery::defaults()));

    foreach ($event->getRecipients() as $recipient) {
      if (FALSE === $query?->doesObjectImplementRequiredInterface($recipient)) {
        $event->removeRecipient($recipient);
      }
    }

    return $event->getRecipients();
  }

}
