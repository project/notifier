<?php

declare(strict_types=1);

namespace Drupal\notifier\Recipients;

/**
 * @internal
 *
 * @phpstan-import-type FieldMapping from \Drupal\notifier\NotifierCompilerPass
 */
final class BundleFieldConfiguration {

  public function __construct(
    public readonly string $entityTypeId,
    public readonly string $bundle,
    public readonly string $fieldName,
  ) {
  }

  /**
   * @phpstan-param FieldMapping $map
   */
  public static function fromRaw(array $map): static {
    return new static(
      $map['entity_type'],
      $map['bundle'],
      $map['field_name'],
    );
  }

}
