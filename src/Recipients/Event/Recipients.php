<?php

declare(strict_types=1);

namespace Drupal\notifier\Recipients\Event;

use Drupal\notifier\Recipients\RecipientQuery;
use Drupal\notifier\Recipients\Recipients as RecipientCollection;
use Symfony\Component\Notifier\Recipient\RecipientInterface;

/**
 * Event for collecting recipients of an object.
 */
final class Recipients {

  private RecipientCollection $recipients;

  public function __construct(
    public readonly object $for,
    public readonly RecipientQuery $query,
  ) {
    $this->recipients = new RecipientCollection();
  }

  /**
   * @phpstan-return $this
   */
  public function addRecipient(RecipientInterface $recipient) {
    $this->recipients[] = $recipient;

    return $this;
  }

  /**
   * @phpstan-return $this
   */
  public function removeRecipient(RecipientInterface $recipient) {
    foreach ($this->recipients as $k => $v) {
      if ($v === $recipient) {
        unset($this->recipients[$k]);
      }
    }

    return $this;
  }

  /**
   * @phpstan-return iterable<\Symfony\Component\Notifier\Recipient\RecipientInterface>
   */
  public function getRecipients(): iterable {
    return $this->recipients;
  }

}
