<?php

declare(strict_types=1);

namespace Drupal\notifier\ChannelRouting;

use Drupal\notifier\ChannelRouting\Attribute\AsChannelRouter;
use Drupal\notifier\ChannelRouting\Channels\Channels;
use Symfony\Component\Notifier\Notification\Notification;
use Symfony\Component\Notifier\Recipient\RecipientInterface;

#[AsChannelRouter(priority: -2000)]
final readonly class Supports implements ChannelRoutingInterface {

  /**
   * @internal
   */
  public function __construct() {
  }

  public function channels(Channels $channels, Notification $notification, RecipientInterface $recipient): void {
    foreach ($channels as $channel) {
      if (FALSE === $channels->getChannel($channel)->supports($notification, $recipient)) {
        $channels->remove($channel);
      }
    }
  }

}
