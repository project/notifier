<?php

declare(strict_types=1);

namespace Drupal\notifier\ChannelRouting\Attribute;

#[\Attribute(\Attribute::TARGET_CLASS)]
final readonly class AsChannelRouter {

  public function __construct(
    public int $priority = 0,
  ) {
  }

}
