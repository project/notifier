<?php

declare(strict_types=1);

namespace Drupal\notifier\ChannelRouting;

use Drupal\notifier\ChannelRouting\Attribute\AsChannelRouter;
use Drupal\notifier\ChannelRouting\Channels\Channels;
use Symfony\Component\Notifier\Notification\Notification;
use Symfony\Component\Notifier\Recipient\RecipientInterface;

#[AsChannelRouter]
final readonly class DefaultChannelRouting implements ChannelRoutingInterface {

  /**
   * @internal
   */
  public function __construct() {}

  public function channels(Channels $channels, Notification $notification, RecipientInterface $recipient): void {
    // Allow an individual notification to override channels.
    $newChannels = $notification->getChannels($recipient);
    if ($newChannels === []) {
      return;
    }

    $newChannels = Channels::create(data: $newChannels);
    foreach ($channels->diff($newChannels) as $removeChannel) {
      $channels->remove($removeChannel);
    }

    foreach ($newChannels as $newChannel) {
      if (FALSE === $channels->contains($newChannel)) {
        $channels[] = $newChannel;
      }
    }
  }

}
