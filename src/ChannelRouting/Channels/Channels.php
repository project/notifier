<?php

declare(strict_types=1);

namespace Drupal\notifier\ChannelRouting\Channels;

use Ramsey\Collection\AbstractCollection;
use Ramsey\Collection\CollectionInterface;
use Symfony\Component\Notifier\Channel\ChannelInterface;

/**
 * @extends \Ramsey\Collection\AbstractCollection<\Drupal\notifier\ChannelRouting\Channels\Channel>
 * @phpstan-type ChannelGetter (callable(string): \Symfony\Component\Notifier\Channel\ChannelInterface)|null
 */
final class Channels extends AbstractCollection {

  /**
   * @var ChannelGetter
   */
  private $channelGetter;

  public function getType(): string {
    return '\\Drupal\\notifier\\ChannelRouting\\Channels\\Channel';
  }

  public function getChannel(Channel $channel): ChannelInterface {
    return ($this->channelGetter ?? throw new \LogicException('Channel getter not set'))($channel->channel);
  }

  /**
   * @phpstan-param array<string|Channel> $data
   * @phpstan-param ChannelGetter $channelGetter
   * @internal
   */
  public static function create(
    array $data = [],
    ?callable $channelGetter = NULL,
  ): static {
    foreach ($data as $k => $value) {
      if (\is_string($value)) {
        $data[$k] = Channel::fromString($value);
      }
    }
    /** @var array<\Drupal\notifier\ChannelRouting\Channels\Channel> $data */
    $obj = new static($data);
    $obj->channelGetter = $channelGetter;
    return $obj;
  }

  public function contains(mixed $element, bool $strict = TRUE): bool {
    \assert($element instanceof Channel);

    $hash = static fn (Channel $channel): string => $channel->channel . '|' . ($channel->transport ?? '');

    return \in_array($hash($element), $this->map($hash)->toArray(), TRUE);
  }

  public function diff(CollectionInterface $other): CollectionInterface {
    $hash = static fn (Channel $channel): string => $channel->channel . '|' . ($channel->transport ?? '');
    $hashesInOther = $other->map($hash)->toArray();

    $diff = new static();
    foreach ($this as $channel) {
      $channelHash = $hash($channel);
      if (!\in_array($channelHash, $hashesInOther, TRUE)) {
        $diff[] = $channel;
      }
    }

    return $diff;
  }

}
