<?php

declare(strict_types=1);

namespace Drupal\notifier\ChannelRouting\Channels;

final class Channel {

  /**
   * @phpstan-param non-empty-string $channel
   * @phpstan-param non-empty-string|null $transport
   */
  private function __construct(
    public string $channel,
    public ?string $transport = NULL,
  ) {
  }

  /**
   * @throws \InvalidArgumentException
   */
  public static function fromString(string $channelWithOptionalTransport): static {
    // Inspired by \Symfony\Component\Notifier\Notifier::getChannels.
    [$channel, $transport] = \str_contains($channelWithOptionalTransport, '/')
      ? \explode('/', $channelWithOptionalTransport, 2)
      : [$channelWithOptionalTransport, NULL];

    if ($transport === '') {
      $transport = NULL;
    }

    if ($channel === '' || $channel === NULL) {
      throw new \InvalidArgumentException('Channel name cannot be empty.');
    }

    return new static($channel, $transport);
  }

}
