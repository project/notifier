<?php

declare(strict_types=1);

namespace Drupal\notifier\ChannelRouting;

use Drupal\notifier\ChannelRouting\Channels\Channels;
use Symfony\Component\Notifier\Notification\Notification;
use Symfony\Component\Notifier\Recipient\RecipientInterface;

interface ChannelRoutingInterface {

  public function channels(
    Channels $channels,
    Notification $notification,
    RecipientInterface $recipient,
  ): void;

}
