<?php

declare(strict_types=1);

namespace Drupal\notifier\ChannelRouting;

use Drupal\notifier\ChannelRouting\Attribute\AsChannelRouter;
use Drupal\notifier\ChannelRouting\Channels\Channels;
use Symfony\Component\Notifier\Channel\ChannelPolicyInterface;
use Symfony\Component\Notifier\Notification\Notification;
use Symfony\Component\Notifier\Recipient\RecipientInterface;

#[AsChannelRouter(priority: -1000)]
final readonly class Policy implements ChannelRoutingInterface {

  /**
   * @internal
   */
  public function __construct(
    private ChannelPolicyInterface $policy,
  ) {
  }

  public function channels(Channels $channels, Notification $notification, RecipientInterface $recipient): void {
    $importanceChannels = $this->policy->getChannels($notification->getImportance());
    if ($importanceChannels === []) {
      // Notifier would quit here, but we don't care if no importance channels
      // are configured.
      return;
    }

    $importanceChannels = Channels::create($importanceChannels);
    // Remove channels, but don't add missing channels.
    foreach ($channels->diff($importanceChannels) as $removeChannel) {
      $channels->remove($removeChannel);
    }
  }

}
