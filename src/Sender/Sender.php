<?php

declare(strict_types=1);

namespace Drupal\notifier\Sender;

use Drupal\notifier\Recipients\NotifierRecipientsInterface;
use Symfony\Component\Notifier\Notification\Notification;
use Symfony\Component\Notifier\NotifierInterface;

/**
 * Service to send notifications objects/entities.
 *
 * This service is a convenience for when code needs to simply send messages to
 * an objects or entities, and your code would rather not concern itself with
 * how recipient data is stored or located for an object/entity.
 *
 * Alternatively, if you want more control. You can construct messages (aka,
 * notifications), recipients, and send the message via Notifier service.
 * See the README. This is what this service does under the hood.
 */
final class Sender implements SenderInterface {

  /**
   * @internal
   */
  public function __construct(
    private readonly NotifierInterface $notifier,
    private readonly NotifierRecipientsInterface $notifierRecipients,
  ) {
  }

  public function send(object $for, Notification $notification, ?QueryOptions $options = NULL): void {
    $recipients = \iterator_to_array($this->notifierRecipients->getRecipients($for));

    $this->notifier->send($notification, ...$recipients);
  }

}
