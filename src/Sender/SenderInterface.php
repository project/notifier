<?php

declare(strict_types=1);

namespace Drupal\notifier\Sender;

use Symfony\Component\Notifier\Notification\Notification;

interface SenderInterface {

  public function send(object $for, Notification $notification, ?QueryOptions $options = NULL): void;

}
