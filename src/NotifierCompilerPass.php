<?php

declare(strict_types=1);

namespace Drupal\notifier;

use Drupal\notifier\Recipients\BundleFieldConfiguration;
use Drupal\notifier\Recipients\EventListener\EntityEmails;
use Drupal\notifier\Recipients\EventListener\EntityPhoneNumbers;
use Drupal\notifier\Recipients\EventListener\UserEmail;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Compiler\ServiceLocatorTagPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Notifier compiler pass.
 *
 * @phpstan-type FieldMapping array{entity_type: non-empty-string, bundle: non-empty-string, field_name: non-empty-string}
 */
final class NotifierCompilerPass implements CompilerPassInterface {

  public function process(ContainerBuilder $container): void {
    // Do it here since Drupal's support for
    // !tagged_locator('notifier.channel', 'channel') is wonky.
    // vendor/symfony/framework-bundle/Resources/config/notifier.php:39.
    $references = [];
    foreach ($container->findTaggedServiceIds('notifier.channel') as $serviceId => $tags) {
      // By 'channel'.
      $channel = $tags[0]['channel'] ?? NULL;
      if ($channel === NULL) {
        throw new \LogicException('Notifier channel tag not found.');
      }

      $references[$channel] = new Reference($serviceId);
    }

    $container->getDefinition('notifier')
      ->setArgument('$channels', ServiceLocatorTagPass::register($container, $references));

    // Notifier module services:
    // Email content entity field mapping:
    /** @var array<FieldMapping> $mappingRaw */
    $mappingRaw = $container->getParameter('notifier.field_mapping.email');
    $mapping = [];
    foreach ($mappingRaw as $map) {
      $mapping[] = BundleFieldConfiguration::fromRaw($map);
    }
    $def = $container->getDefinition(EntityEmails::class);
    $def->setArgument('$configurations', \serialize($mapping));

    // Phone number content entity field mapping:
    /** @var array<FieldMapping> $mappingRaw */
    $mappingRaw = $container->getParameter('notifier.field_mapping.sms');
    $mapping = [];
    foreach ($mappingRaw as $map) {
      $mapping[] = BundleFieldConfiguration::fromRaw($map);
    }
    $def = $container->getDefinition(EntityPhoneNumbers::class);
    $def->setArgument('$configurations', \serialize($mapping));

    // User entity email field mapping:
    // Removes the event subscriber service if turned off.
    if ($container->getParameter('notifier.field_mapping.user_email') !== TRUE) {
      $container->removeDefinition(UserEmail::class);
    }
  }

}
