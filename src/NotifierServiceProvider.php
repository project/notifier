<?php

declare(strict_types=1);

namespace Drupal\notifier;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderInterface;

/**
 * Service provider for Notifier.
 */
final class NotifierServiceProvider implements ServiceProviderInterface {

  public function register(ContainerBuilder $container): void {
    $container
      // Priority 200 to execute before AttributeAutoconfigurationPass which has
      // priority 100. See symfony/dependency-injection/Compiler/PassConfig.php.
      ->addCompilerPass(new NotifierChannelRouterCompilerPass(), priority: 200)
      ->addCompilerPass(new NotifierCompilerPass());
  }

}
