<?php

declare(strict_types=1);

namespace Drupal\notifier;

use Drupal\notifier\ChannelRouting\Attribute\AsChannelRouter;
use Symfony\Component\DependencyInjection\ChildDefinition;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Notifier channel router compiler pass.
 */
final class NotifierChannelRouterCompilerPass implements CompilerPassInterface {

  public function process(ContainerBuilder $container): void {
    $container->registerAttributeForAutoconfiguration(AsChannelRouter::class, static function (ChildDefinition $definition, AsChannelRouter $attribute): void {
      $tagAttributes = \get_object_vars($attribute);
      $definition->addTag('notifier.channel_routing', $tagAttributes);
    });
  }

}
