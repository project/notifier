<?php

declare(strict_types=1);

namespace Drupal\notifier_entity_test\Entity;

use Drupal\bca\Attribute\Bundle;
use Drupal\entity_test\Entity\EntityTest;
use Symfony\Component\Notifier\Recipient\EmailRecipientInterface;

#[Bundle(
  entityType: 'entity_test',
  bundle: 'entity_test',
)]
final class NotifierTestEntity extends EntityTest implements EmailRecipientInterface {

  public function getEmail(): string {
    return 'notifier-test-entity@example.com';
  }

}
