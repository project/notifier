<?php

declare(strict_types=1);

namespace Drupal\notifier_test;

use Symfony\Component\Notifier\Recipient\RecipientInterface;

interface NotifierTestRecipientInterface extends RecipientInterface {

  public function getIdentifier(): string;

}
