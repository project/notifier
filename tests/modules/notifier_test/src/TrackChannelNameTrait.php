<?php

declare(strict_types=1);

namespace Drupal\notifier_test;

trait TrackChannelNameTrait {

  public string $channelName;

  /**
   * Set channel name so messages can be put in the correct buffer.
   */
  public function setChannelName(string $channelName): void {
    $this->channelName = $channelName;
  }

}
