<?php

declare(strict_types=1);

namespace Drupal\notifier_test;

use Drupal\Core\State\StateInterface;
use Symfony\Component\Notifier\Message\MessageInterface;

final class NotifierTestMessageBuffer {


  public const STATE = 'notifier_test_messages';

  public function __construct(
    private StateInterface $state,
  ) {
  }

  public function push(string $channelName, MessageInterface $message): void {
    /** @var array<string, \Symfony\Component\Notifier\Message\MessageInterface[]> $buffer */
    $buffer = $this->state->get(static::STATE, []);
    $buffer[$channelName][] = $message;
    $this->state->set(static::STATE, $buffer);
  }

  /**
   * @phpstan-return array<string, \Symfony\Component\Notifier\Message\MessageInterface[]>
   */
  public function getAndClear(): array {
    /** @var array<string, \Symfony\Component\Notifier\Message\MessageInterface[]> $buffer */
    $buffer = $this->state->get(static::STATE, []);
    $this->state->delete(static::STATE);
    return $buffer;
  }

}
