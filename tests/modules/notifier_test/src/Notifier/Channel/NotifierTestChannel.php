<?php

declare(strict_types=1);

namespace Drupal\notifier_test\Notifier\Channel;

use Drupal\notifier_test\Notifier\Message\NotifierTestMessage;
use Drupal\notifier_test\NotifierTestRecipientInterface;
use Drupal\notifier_test\TrackChannelNameTrait;
use Symfony\Component\Notifier\Channel\AbstractChannel;
use Symfony\Component\Notifier\Notification\Notification;
use Symfony\Component\Notifier\Recipient\RecipientInterface;

final class NotifierTestChannel extends AbstractChannel {

  use TrackChannelNameTrait;

  public function supports(Notification $notification, RecipientInterface $recipient): bool {
    return $recipient instanceof NotifierTestRecipientInterface
      && $recipient->getIdentifier() !== 'deny recipient';
  }

  public function notify(Notification $notification, RecipientInterface $recipient, ?string $transportName = NULL): void {
    if (FALSE === $recipient instanceof NotifierTestRecipientInterface) {
      // Impossible, validated by supports().
      throw new \LogicException();
    }

    $message = NotifierTestMessage::fromNotification($notification, $recipient);
    $message->setChannelName($this->channelName);

    if (NULL !== $transportName) {
      $message->transport($transportName);
    }

    $this->transport?->send($message);
  }

}
