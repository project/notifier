<?php

declare(strict_types=1);

namespace Drupal\notifier_test\Notifier\Transport;

use Drupal\notifier_test\Notifier\Message\NotifierTestMessage;
use Drupal\notifier_test\NotifierTestMessageBuffer;
use Symfony\Component\Notifier\Message\MessageInterface;
use Symfony\Component\Notifier\Message\SentMessage;
use Symfony\Component\Notifier\Transport\TransportInterface;

final class NotifierTestTransport implements TransportInterface {

  public function __construct(
    private NotifierTestMessageBuffer $buffer,
  ) {
  }

  public function __toString(): string {
    return 'notifier_test transport';
  }

  public function send(MessageInterface $message): ?SentMessage {
    \assert($message instanceof NotifierTestMessage);
    $this->buffer->push($message->channelName, $message);

    return new SentMessage($message, (string) $this);
  }

  public function supports(MessageInterface $message): bool {
    return TRUE;
  }

}
