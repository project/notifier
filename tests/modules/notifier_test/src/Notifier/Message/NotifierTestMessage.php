<?php

declare(strict_types=1);

namespace Drupal\notifier_test\Notifier\Message;

use Drupal\notifier_test\NotifierTestRecipientInterface;
use Drupal\notifier_test\TrackChannelNameTrait;
use Symfony\Component\Notifier\Message\MessageInterface;
use Symfony\Component\Notifier\Message\MessageOptionsInterface;
use Symfony\Component\Notifier\Notification\Notification;

final class NotifierTestMessage implements MessageInterface {

  use TrackChannelNameTrait;

  private ?string $transport = NULL;

  private function __construct(
    public string $subject,
    public string $toIdentifier,
  ) {
  }

  public static function fromNotification(Notification $notification, NotifierTestRecipientInterface $recipient): self {
    $message = new self(
      subject: $notification->getSubject(),
      toIdentifier: $recipient->getIdentifier(),
    );
    return $message;
  }

  /**
   * @return $this
   */
  public function transport(?string $transport): static {
    $this->transport = $transport;

    return $this;
  }

  public function getRecipientId(): ?string {
    return NULL;
  }

  public function getSubject(): string {
    return $this->subject;
  }

  public function getOptions(): ?MessageOptionsInterface {
    return NULL;
  }

  public function getTransport(): ?string {
    return $this->transport;
  }

}
