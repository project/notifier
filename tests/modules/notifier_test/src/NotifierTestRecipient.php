<?php

declare(strict_types=1);

namespace Drupal\notifier_test;

final class NotifierTestRecipient implements NotifierTestRecipientInterface {

  public function __construct(
    private readonly string $identifier,
  ) {
  }

  public function getIdentifier(): string {
    return $this->identifier;
  }

}
