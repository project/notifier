<?php

declare(strict_types=1);

namespace Drupal\Tests\notifier\Unit;

use Drupal\notifier\ChannelRouting\Channels\Channel;
use Drupal\notifier\ChannelRouting\Channels\Channels;
use Drupal\Tests\UnitTestCase;

final class NotifierUnitTest extends UnitTestCase {

  /**
   * @covers \Drupal\notifier\ChannelRouting\Channels\Channels::contains
   */
  public function testContains(): void {
    $channels = Channels::create(
      data: [
        Channel::fromString('foo'),
        Channel::fromString('hello/world'),
      ],
    );

    self::assertTrue($channels->contains(Channel::fromString('foo')));
    self::assertFalse($channels->contains(Channel::fromString('foo/bar')));
    self::assertFalse($channels->contains(Channel::fromString('hello')));
    self::assertTrue($channels->contains(Channel::fromString('hello/world')));
  }

  /**
   * @covers \Drupal\notifier\ChannelRouting\Channels\Channels::diff
   */
  public function testDiff(): void {
    $channels = Channels::create(
      data: [
        Channel::fromString('foo'),
        Channel::fromString('foo/bar'),
        Channel::fromString('hello/world'),
      ],
    );

    $other = Channels::create(
      data: [
        Channel::fromString('foo'),
      ],
    );

    self::assertEquals(Channels::create(
      data: [
        Channel::fromString('foo/bar'),
        Channel::fromString('hello/world'),
      ],
    ), $channels->diff($other));
  }

}
