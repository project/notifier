<?php

declare(strict_types=1);

namespace Drupal\Tests\notifier\Kernel;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\KernelTests\KernelTestBase;
use Drupal\notifier_test\NotifierTestMessageBuffer;
use Drupal\notifier_test\NotifierTestRecipient;
use Symfony\Component\Notifier\Notification\Notification;
use Symfony\Component\Notifier\NotifierInterface;

final class NotifierTest extends KernelTestBase {

  protected static $modules = [
    'notifier',
    'notifier_test',
  ];

  public function testNotifier(): void {
    $recipient = new NotifierTestRecipient('user@example.com');

    $notification = (new Notification())
      ->subject('Test message');

    static::notifier()->send($notification, $recipient);

    /** @var array<string, \Drupal\notifier_test\Notifier\Message\NotifierTestMessage[]> $buffer */
    $buffer = static::buffer()->getAndClear();
    static::assertCount(1, $buffer);
    static::assertEquals('Test message', $buffer['notifier_test_channel'][0]->getSubject());
    static::assertEquals('user@example.com', $buffer['notifier_test_channel'][0]->toIdentifier);
  }

  public function register(ContainerBuilder $container): void {
    parent::register($container);

    $container->setParameter('notifier.channel_policy', [
      'urgent' => ['notifier_test_channel'],
      'high' => ['notifier_test_channel'],
      'medium' => ['notifier_test_channel'],
      'low' => ['notifier_test_channel'],
    ]);
  }

  private static function notifier(): NotifierInterface {
    /** @var \Symfony\Component\Notifier\NotifierInterface */
    return \Drupal::service(NotifierInterface::class);
  }

  private static function buffer(): NotifierTestMessageBuffer {
    /** @var \Drupal\notifier_test\NotifierTestMessageBuffer */
    return \Drupal::service(NotifierTestMessageBuffer::class);
  }

}
