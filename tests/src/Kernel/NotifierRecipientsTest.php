<?php

declare(strict_types=1);

namespace Drupal\Tests\notifier\Kernel;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\Extension\ModuleInstallerInterface;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\KernelTests\KernelTestBase;
use Drupal\notifier\Recipients\NotifierRecipientsInterface;
use Drupal\notifier\Recipients\RecipientQuery;
use Drupal\notifier_entity_test\Entity\NotifierTestEntity;
use Drupal\user\Entity\User;
use Symfony\Component\Notifier\Recipient\EmailRecipientInterface;
use Symfony\Component\Notifier\Recipient\SmsRecipientInterface;

final class NotifierRecipientsTest extends KernelTestBase {

  protected static $modules = [
    'bca',
    'entity_test',
    'field',
    'notifier',
    'notifier_test',
    'telephone',
    'user',
  ];

  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('entity_test');

    FieldStorageConfig::create([
      'field_name' => 'phone_number',
      'type' => 'telephone',
      'entity_type' => 'entity_test',
    ])->save();
    FieldConfig::create([
      'field_name' => 'phone_number',
      'entity_type' => 'entity_test',
      'bundle' => 'entity_test',
    ])->save();

    FieldStorageConfig::create([
      'field_name' => 'email',
      'type' => 'email',
      'entity_type' => 'entity_test',
    ])->save();
    FieldConfig::create([
      'field_name' => 'email',
      'entity_type' => 'entity_test',
      'bundle' => 'entity_test',
    ])->save();
  }

  /**
   * @covers \Drupal\notifier\Recipients\EventListener\UserEmail
   */
  public function testUserEmail(): void {
    $for = User::create([
      'name' => 'test',
      'mail' => 'foo@bar.com',
    ]);
    $recipients = \iterator_to_array(static::recipients()->getRecipients($for));
    static::assertCount(1, $recipients);
    static::assertInstanceOf(EmailRecipientInterface::class, $recipients[0]);
    static::assertEquals('foo@bar.com', $recipients[0]->getEmail());
  }

  /**
   * @covers \Drupal\notifier\Recipients\EventListener\EntityPhoneNumbers
   */
  public function testEntityPhoneNumbers(): void {
    $for = EntityTest::create([
      'phone_number' => '+123',
    ]);
    $recipients = \iterator_to_array(static::recipients()->getRecipients($for));
    static::assertCount(1, $recipients);
    static::assertInstanceOf(SmsRecipientInterface::class, $recipients[0]);
    static::assertEquals('+123', $recipients[0]->getPhone());
  }

  /**
   * @covers \Drupal\notifier\Recipients\EventListener\EntityEmails
   */
  public function testEntityEmail(): void {
    $for = EntityTest::create([
      'email' => 'foo@example.com',
    ]);
    $recipients = \iterator_to_array(static::recipients()->getRecipients($for));
    static::assertCount(1, $recipients);
    static::assertInstanceOf(EmailRecipientInterface::class, $recipients[0]);
    static::assertEquals('foo@example.com', $recipients[0]->getEmail());
  }

  /**
   * Tests bundle classes/plain objects.
   *
   * @covers \Drupal\notifier\Recipients\EventListener\Recipient
   */
  public function testRecipientInterface(): void {
    \Drupal::service(ModuleInstallerInterface::class)->install(['notifier_entity_test']);
    $for = NotifierTestEntity::create([]);

    $recipients = \iterator_to_array(static::recipients()->getRecipients($for));
    static::assertCount(1, $recipients);
    static::assertInstanceOf(EmailRecipientInterface::class, $recipients[0]);
    static::assertEquals('notifier-test-entity@example.com', $recipients[0]->getEmail());
  }

  /**
   * @covers \Drupal\notifier\Recipients\RecipientQuery::__construct(implementing)
   */
  public function testImplementsQuery(): void {
    $for = EntityTest::create([
      'email' => 'foo@example.com',
      'phone_number' => '+123',
    ]);

    // No query.
    $recipients = \iterator_to_array(static::recipients()->getRecipients($for));
    static::assertCount(2, $recipients);
    static::assertInstanceOf(EmailRecipientInterface::class, $recipients[0]);
    static::assertInstanceOf(SmsRecipientInterface::class, $recipients[1]);

    $recipients = \iterator_to_array(static::recipients()->getRecipients($for, query: new RecipientQuery(
      implementing: EmailRecipientInterface::class,
    )));
    static::assertCount(1, $recipients);
    static::assertInstanceOf(EmailRecipientInterface::class, $recipients[0]);

    $recipients = \iterator_to_array(static::recipients()->getRecipients($for, query: new RecipientQuery(
      implementing: SmsRecipientInterface::class,
    )));
    static::assertCount(1, $recipients);
    static::assertInstanceOf(SmsRecipientInterface::class, $recipients[1]);
  }

  public function register(ContainerBuilder $container): void {
    parent::register($container);

    if (\in_array($this->name(), ['testEntityPhoneNumbers', 'testImplementsQuery'], TRUE)) {
      $container->setParameter('notifier.field_mapping.sms', [
        [
          'entity_type' => 'entity_test',
          'bundle' => 'entity_test',
          'field_name' => 'phone_number',
        ],
      ]);
    }

    if (\in_array($this->name(), ['testEntityEmail', 'testImplementsQuery'], TRUE)) {
      $container->setParameter('notifier.field_mapping.email', [
        [
          'entity_type' => 'entity_test',
          'bundle' => 'entity_test',
          'field_name' => 'email',
        ],
      ]);
    }
  }

  private static function recipients(): NotifierRecipientsInterface {
    /** @var \Drupal\notifier\Recipients\NotifierRecipientsInterface */
    return \Drupal::service(NotifierRecipientsInterface::class);
  }

}
