<?php

declare(strict_types=1);

namespace Drupal\Tests\notifier\Kernel;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\KernelTests\KernelTestBase;
use Drupal\notifier_test\NotifierTestMessageBuffer;
use Drupal\notifier_test\NotifierTestRecipient;
use Symfony\Component\DependencyInjection\ChildDefinition;
use Symfony\Component\Notifier\Notification\Notification;
use Symfony\Component\Notifier\NotifierInterface;

final class NotifierRoutingTest extends KernelTestBase {

  protected static $modules = [
    'notifier',
    'notifier_test',
  ];

  public function testNotifierRouting(): void {
    $recipient = new NotifierTestRecipient('user@example.com');

    $notification = (new Notification())
      ->subject('Test message');

    static::notifier()->send($notification, $recipient);

    /** @var array<string, \Drupal\notifier_test\Notifier\Message\NotifierTestMessage[]> $buffer */
    $buffer = static::buffer()->getAndClear();
    static::assertCount(2, $buffer);
  }

  public function testOverrideOnNotification(): void {
    $recipient = new NotifierTestRecipient('user@example.com');

    $notification = (new Notification())
      ->subject('Test message')
      // Override here.
      ->channels(['notifier_test_channel_2']);

    static::notifier()->send($notification, $recipient);

    /** @var array<string, \Drupal\notifier_test\Notifier\Message\NotifierTestMessage[]> $buffer */
    $buffer = static::buffer()->getAndClear();
    static::assertCount(1, $buffer);
  }

  /**
   * @phpstan-param Notification::IMPORTANCE_* $importance
   * @dataProvider providerNotifierRoutingByChannelPolicy
   * @covers \Drupal\notifier\ChannelRouting\Policy
   */
  public function testByChannelPolicy(string $importance, bool $expectInChannel1, bool $expectInChannel2): void {
    $recipient = new NotifierTestRecipient('user@example.com');

    $notification = (new Notification())
      ->subject('Test message')
      ->importance($importance);

    static::notifier()->send($notification, $recipient);

    /** @var array<string, \Drupal\notifier_test\Notifier\Message\NotifierTestMessage[]> $buffer */
    $buffer = static::buffer()->getAndClear();
    static::assertCount($expectInChannel1 ? 1 : 0, $buffer['notifier_test_channel'] ?? []);
    static::assertCount($expectInChannel2 ? 1 : 0, $buffer['notifier_test_channel_2'] ?? []);
  }

  public static function providerNotifierRoutingByChannelPolicy(): \Generator {
    // The true/false channel mapping is per register().
    yield [Notification::IMPORTANCE_HIGH, FALSE, TRUE];
    yield [Notification::IMPORTANCE_MEDIUM, TRUE, FALSE];
    yield [Notification::IMPORTANCE_LOW, TRUE, TRUE];
  }

  /**
   * @covers \Drupal\notifier\ChannelRouting\Supports
   */
  public function testSupports(): void {
    foreach ([
      ['user@example.com', 2],
      // See NotifierTestChannel::supports.
      ['deny recipient', 0],
    ] as [$identifier, $expectCount]) {
      $recipient = new NotifierTestRecipient($identifier);

      $notification = (new Notification())
        ->subject('Test message');

      static::notifier()->send($notification, $recipient);

      /** @var array<string, \Drupal\notifier_test\Notifier\Message\NotifierTestMessage[]> $buffer */
      $buffer = static::buffer()->getAndClear();
      static::assertCount($expectCount, $buffer, message: $identifier);
    }
  }

  public function register(ContainerBuilder $container): void {
    parent::register($container);

    if ($this->name() === 'testByChannelPolicy') {
      $container->setParameter('notifier.channel_policy', [
        // Only messages with these will be allowed.
        Notification::IMPORTANCE_HIGH => ['notifier_test_channel_2'],
        Notification::IMPORTANCE_MEDIUM => ['notifier_test_channel'],
        // Nothing = channel policy isn't applied. So all valid channels from
        // previous channel routers flow through.
        Notification::IMPORTANCE_LOW => [],
      ]);
    }

    $container->setDefinition('.notifier_test.channel_2', (new ChildDefinition('notifier_test.channel'))
      ->addTag('notifier.channel', ['channel' => 'notifier_test_channel_2'])
      ->addMethodCall('setChannelName', ['notifier_test_channel_2']),
    );
  }

  private static function notifier(): NotifierInterface {
    /** @var \Symfony\Component\Notifier\NotifierInterface */
    return \Drupal::service(NotifierInterface::class);
  }

  private static function buffer(): NotifierTestMessageBuffer {
    /** @var \Drupal\notifier_test\NotifierTestMessageBuffer */
    return \Drupal::service(NotifierTestMessageBuffer::class);
  }

}
